//
//  APIRequest.swift
//  MujigaeTechTest
//
//  Created by AIA-Chris on 03/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import Foundation
import UIKit

class APIRequest {
    
    public static func requestMenu(url:String, completion:@escaping (_ menu:Menu?, _ error:String?) -> Void) {
        let session = URLSession.shared
        let url = URL(string: url)!
        let task = session.dataTask(with: url, completionHandler: { data, response, error in
            if (try? JSONSerialization.jsonObject(with: data!, options: [])) != nil {
                let menuObject = try? JSONDecoder().decode(Menu.self, from: data!)
                completion(menuObject, nil)
            }
        })
        task.resume()
    }
    
    public static func postFavoritesMenu(ids:[Int], completion:@escaping (_ success:Bool?, _ error:String?) -> Void) {
        let session = URLSession.shared
        let url = URL(string: "http://demo4617705.mockable.io/favorites")!
        let task = session.dataTask(with: url, completionHandler: { data, response, error in
                if data != nil {
                    completion(true, nil)
                }
        })
        task.resume()
    }
}
