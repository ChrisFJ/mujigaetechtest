//
//  StringExtension.swift
//  MujigaeTechTest
//
//  Created by AIA-Chris on 03/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit

extension String {
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
}
