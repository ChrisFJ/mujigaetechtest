//
//  FontExtension.swift
//  MujigaeTechTest
//
//  Created by AIA-Chris on 03/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit
extension UIFont {
    public class func ProximaNovaReguler(with size:CGFloat) -> UIFont {
        return UIFont(name: "ProximaNova-Regular", size: size)!
    }
    public class func ProximaNovaBold(with size:CGFloat) -> UIFont {
        return UIFont(name: "ProximaNova-Bold", size: size)!
    }
    public class func ProximaNovaSemibold(with size:CGFloat) -> UIFont {
        return UIFont(name: "ProximaNova-Semibold", size: size)!
    }
}
