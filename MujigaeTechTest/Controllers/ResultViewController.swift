//
//  ResultViewController.swift
//  MujigaeTechTest
//
//  Created by AIA-Chris on 04/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didCloseTapped() {
        self.dismiss(animated: true, completion: nil)
    }

}
