//
//  ViewController.swift
//  MujigaeTechTest
//
//  Created by AIA-Chris on 03/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit

class MainController: UIViewController {

    @IBOutlet weak var collectionView:UICollectionView!

    let viewModel = MainViewModel()
    let columnLayout = LeftAlignedCollectionViewFlowLayout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib.init(nibName: "MenuCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        collectionView.dataSource = viewModel
        collectionView.delegate = viewModel
        collectionView.allowsMultipleSelection = true
        collectionView.collectionViewLayout = columnLayout
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
        viewModel.getMenus { (success, error) in
            if success! {
                self.collectionView.reloadData()
            }
        }
    }
    
    @IBAction func didDoneButtonTapped() {
        if viewModel.selectedMenu.count > 0 {
            APIRequest.postFavoritesMenu(ids: viewModel.selectedMenu) { (success, error) in
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "resultvc")
                    self.present(vc!, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
    }
}

