//
//  MainViewModel.swift
//  MujigaeTechTest
//
//  Created by AIA-Chris on 04/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit

class MainViewModel: NSObject, UICollectionViewDelegate, UICollectionViewDataSource {
    
    private var menus = [MenuElement]()
    internal var selectedMenu = [Int]()
    
    func getMenus(completion:@escaping (_ success:Bool?, _ error:String?) -> Void) {
        APIRequest.requestMenu(url: "http://www.mocky.io/v2/5cf4f8352f0000c4264f05c0") { (menu, error) in
            if error != nil {
                print(error!)
                completion(false, error)
                return
            }
            self.menus = (menu?.menu)!
            DispatchQueue.main.async {
                completion(true, nil)
            }
        }
    }
    //CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menus.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MenuCell
        cell.bind(menu: menus[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let menu = menus[indexPath.row]
        selectedMenu.append(menu.id!)
        print(selectedMenu)
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let menu = menus[indexPath.row]
        let index = selectedMenu.firstIndex(of: menu.id!)
        selectedMenu.remove(at: index!)
        print(selectedMenu)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var w = collectionView.bounds.size.width / 3
        let cusW = (self.menus[indexPath.row].title?.widthOfString(usingFont: UIFont.ProximaNovaSemibold(with: 13)))! + 32
        if w < cusW {
            w = cusW
        }
        let h = CGFloat(40)
        return CGSize(width: cusW, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 24, left: 15, bottom: 24, right: 15)
    }
}
