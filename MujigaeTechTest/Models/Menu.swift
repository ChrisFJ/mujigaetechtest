//
//  Menu.swift
//  MujigaeTechTest
//
//  Created by AIA-Chris on 03/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import Foundation
class Menu: Codable {
    let resto: String?
    let menu: [MenuElement]?
}

class MenuElement: Codable {
    let title: String?
    let id: Int?
    
    init(title: String?, id: Int?) {
        self.title = title
        self.id = id
    }
}
