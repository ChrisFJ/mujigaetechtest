//
//  MujiButton.swift
//  MujigaeTechTest
//
//  Created by AIA-Chris on 04/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit

class MujiButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = self.bounds.size.height / 2
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = self.bounds.size.height / 2
    }
}
