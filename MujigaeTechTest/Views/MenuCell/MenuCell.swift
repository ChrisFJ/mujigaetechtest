//
//  MenuCell.swift
//  MujigaeTechTest
//
//  Created by AIA-Chris on 03/06/19.
//  Copyright © 2019 Chrizers. All rights reserved.
//

import UIKit

class MenuCell: UICollectionViewCell {

    var isHeightCalculated: Bool = false
    var menu:MenuElement?
    
    @IBOutlet weak var labelTitle:UILabel!
    
    func bind(menu:MenuElement) {
        self.menu = menu
        labelTitle.text = menu.title
    }
    
    override var isSelected: Bool {
        didSet {
            self.layer.borderColor = isSelected ? UIColor(rgb: 0xEBA1A1).cgColor : UIColor(rgb: 0xD9D8D8).cgColor
            self.labelTitle.textColor = isSelected ? UIColor(rgb: 0xEBA1A1) : UIColor(rgb: 0xAAAAAA)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = self.frame.height / 2.5
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor(rgb: 0xD9D8D8).cgColor
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
    }

}
